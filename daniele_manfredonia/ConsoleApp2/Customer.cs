﻿using System;
using System.Text.RegularExpressions;


namespace ConsoleApp2
{
    /// <summary>
    ///  The Class Costumer implements the concept of customer who goes to gym.
    /// </summary>
    public class Customer
    {

        /// <summary>
        /// The enum to represent gender.
        /// </summary>
        public enum Gender
        {
            MALE,
            FEMALE
        }

        /// <summary>
        /// The customer's id.
        /// </summary>
        public String id { get; protected set; }

        /// <summary>
        /// The customer's firstname.
        /// </summary>
        public String firstname { get; protected set; }

        /// <summary>
        /// The customer's lastname.
        /// </summary>
        public String lastname { get; protected set; }

        /// <summary>
        /// The customer's fiscalcode.
        /// </summary>
        public String fiscalcode { get; protected set; }

        /// <summary>
        /// The customer's gender.
        /// </summary>
        public Gender gender { get; protected set; }

        /// <summary>
        /// The customer's birthdate.
        /// </summary>
        public DateTime birthdate { get; protected set; }

        /// <summary>
        /// The customer's email.
        /// </summary>
        public String email { get; protected set; }

        /// <summary>
        /// The customer's telephone number
        /// </summary>
        public String telephoneNumber { get; protected set; }

        /// <summary>
        /// The empty constructor.
        /// </summary>
        private Customer() { }


        /// <summary>
        /// The Class to build the customer.
        /// </summary>
        public class Builder
        {
            private String id;

            private String firstname;

            private String lastname;

            private String fiscalcode;

            private Gender gender;

            private DateTime birthdate;

            private String email;

            private String telephoneNumber;

            /// <summary>
            /// Method to set the customer's id.
            /// </summary>
            /// <param name="id"> The ID </param>
            /// <returns> The builder </returns>
            public Builder ID(String id)
            {
                if (IsIdValid(id))
                {
                    this.id = id;
                }

                return this;
            }

            /// <summary>
            /// Method to set the customer's firstname.
            /// </summary>
            /// <param name="firstname"> The firstname </param>
            /// <returns> The builder </returns>
            public Builder Firstname(String firstname)
            {
                if (IsNameValid(firstname))
                {
                    this.firstname = firstname;
                }

                return this;
            }

            /// <summary>
            /// Method to set the customer's lastname.
            /// </summary>
            /// <param name="lastname"> The lastname </param>
            /// <returns> The builder </returns>
            public Builder Lastname(String lastname)
            {
                if (IsNameValid(lastname))
                {
                    this.lastname = lastname;
                }

                return this;
            }

            /// <summary>
            /// Method to set the customer's fiscalcode
            /// </summary>
            /// <param name="fiscalcode"> The fiscalcode </param>
            /// <returns> The builder </returns>
            public Builder Fiscalcode(String fiscalcode)
            {
                if (IsFiscalcodeValid(fiscalcode))
                {
                    this.fiscalcode = fiscalcode;
                }

                return this;
            }

            /// <summary>
            /// Method to set the customer's gender
            /// </summary>
            /// <param name="gender"> The gender </param>
            /// <returns> The builder </returns>
            public Builder Gender(Gender gender)
            {
                if (!gender.Equals(null))
                {
                    this.gender = gender;
                }

                return this;
            }

            /// <summary>
            /// Method to set the customer's birthdate.
            /// </summary>
            /// <param name="birthdate"> The birthdate </param>
            /// <returns> The builder </returns>
            public Builder Birthdate(DateTime birthdate)
            {
                if (IsBirthdateValid(birthdate))
                {
                    this.birthdate = birthdate;
                }

                return this;
            }

            /// <summary>
            /// Method to set the customer's email.
            /// </summary>
            /// <param name="email"> The email </param>
            /// <returns> The builder </returns>
            public Builder Email(String email)
            {
                if (IsEmailValid(email))
                {
                    this.email = email;
                }

                return this;
            }

            /// <summary>
            /// Method to set the customer's telephone number.
            /// </summary>
            /// <param name="telephoneNumber"> The telephone number </param>
            /// <returns> The builder </returns>
            public Builder TelephoneNumber(String telephoneNumber)
            {
                if (IsTelephoneNumberValid(telephoneNumber))
                {
                    this.telephoneNumber = telephoneNumber;
                }

                return this;
            }

            /// <summary>
            /// Method to build the customer.
            /// </summary>
            /// <returns> The customer built </returns>
            public Customer Build()
            {
                Customer customer = new Customer();

                customer.id = id;
                customer.firstname = firstname;
                customer.lastname = lastname;
                customer.gender = gender;
                customer.birthdate = birthdate;
                customer.fiscalcode = fiscalcode;
                customer.telephoneNumber = telephoneNumber;
                customer.email = email;

                return customer;

            }

        }
        /// <summary>
        /// Method to check if the length of id is correct.
        /// </summary>
        /// <param name="id"></param>
        /// <returns> True if it is correct, otherwise false </returns>
        public static bool IsIdValid(String id)
        {
            return id.Length >= 8;
        }

        /// <summary>
        /// Method to check if the parameter is only literal.
        /// </summary>
        /// <param name="name"> The name </param>
        /// <returns> True if it is correct, otherwise false </returns>
        public static bool IsNameValid(String name)
        {
            String regex = "^[a-zA-Zèéòàùì' ]+$";

            return Regex.IsMatch(name, regex);
        }

        /// <summary>
        /// Method to check if parameter birthdate is before today. 
        /// </summary>
        /// <param name="date"> The birthdate </param>
        /// <returns> True if it is correct, otherwise false </returns>
        public static bool IsBirthdateValid(DateTime date)
        {
            //String date.ToString("d/M/yyyy");
            return DateTime.Compare(DateTime.Now, date) > 0;
        }

        /// <summary>
        /// Method to check the format of fiscalcode.
        /// </summary>
        /// <param name="fiscalcode"> The fiscalcode </param>
        /// <returns> True if it is correct, otherwise false </returns>
        public static bool IsFiscalcodeValid(String fiscalcode)
        {
            String regex = "[a-zA-Z]{6}\\d\\d[a-zA-Z]\\d\\d[a-zA-Z]\\d\\d\\d[a-zA-Z]";

            return Regex.IsMatch(fiscalcode, regex);
        }

        /// <summary>
        /// Method to check the format of email.
        /// </summary>
        /// <param name="email"> The email </param>
        /// <returns> True if it is correct, otherwise false </returns>
        public static bool IsEmailValid(String email)
        {
            String regex = "[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}";

            return Regex.IsMatch(email, regex);
        }

        /// <summary>
        /// Method to check the format of telephone number.
        /// </summary>
        /// <param name="telephoneNumber"> The telephone number </param>
        /// <returns> True if it is correct, otherwise false </returns>
        public static bool IsTelephoneNumberValid(String telephoneNumber)
        {
            String regex = "[0-9]{10}";

            return Regex.IsMatch(telephoneNumber, regex);
        }

    }
}


