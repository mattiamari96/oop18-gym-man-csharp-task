﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    /// <summary>
    /// The Class NumberedRegistration implements the concept of registration with entries.
    /// </summary>
    public class NumberedRegistration : Registration
    {

        /// <summary>
        /// The number of entries for registration.
        /// </summary>
        public int maxEntries { get; protected set; }

        /// <summary>
        /// The count of entries made.
        /// </summary>
        public int entriesCount { get; protected set; }

        /// <summary>
        /// The empty constructor.
        /// </summary>
        private NumberedRegistration() { }

        /// <summary>
        /// The class to build the numberedRegistration.
        /// </summary>
        public class Builder
        {

            private String Idclient;

            private SubscriptionType type;

            private Double discount;

            private int maxEntries;

            private int entriesCount = 0;

            /// <summary>
            /// Method to set registration'id client.
            /// </summary>
            /// <param name="IDclient"> The id client </param>
            /// <returns> The builder </returns>
            public Builder IDclient(String IDclient)
            {
                this.Idclient = IDclient;

                return this;
            }

            /// <summary>
            /// Method to set registration'subscription type.
            /// </summary>
            /// <param name="type"> The subscription type </param>
            /// <returns> The builder </returns>
            public Builder Type(SubscriptionType type)
            {
                if (type != null)
                {
                    this.type = type;
                }

                return this;
            }

            /// <summary>
            /// Method to set registration'discount.
            /// </summary>
            /// <param name="discount"> The discount </param>
            /// <returns> The builder </returns>
            public Builder Discount(Double discount)
            {
                if (IsDiscountValid(discount))
                {
                    this.discount = discount;
                }
                return this;
            }

            /// <summary>
            ///  Method to set registration'entries.
            /// </summary>
            /// <param name="maxEntries"> The entries </param>
            /// <returns> The builder </returns>
            public Builder MaxEntries(int maxEntries)
            {
                if (IsDurationValid(maxEntries))
                {
                    this.maxEntries = maxEntries;
                }
                return this;
            }

            /// <summary>
            /// Method to build the numbered registration.
            /// </summary>
            /// <returns> The new registration </returns>
            public NumberedRegistration Build()
            {
                NumberedRegistration numberedRegistration = new NumberedRegistration();

                numberedRegistration.Idclient = Idclient;
                numberedRegistration.type = type;
                numberedRegistration.maxEntries = maxEntries;
                numberedRegistration.entriesCount = 0;
                numberedRegistration.discount = discount;

                return numberedRegistration;
            }

        }


        public override double GetPrice()
        {
            return this.maxEntries * this.type.price * (1 - this.discount / 100);
        }

        public override bool IsActive(DateTime date)
        {
            return this.entriesCount < this.maxEntries;
        }

        /// <summary>
        ///  Method to add a entry.
        /// </summary>
        public void AddCountEntries()
        {
            this.entriesCount++;
        }
    }
}
