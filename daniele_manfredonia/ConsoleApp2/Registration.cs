﻿using System;


namespace ConsoleApp2
{
    /// <summary>
    /// The Class Registration implements the concept of registration.
    /// </summary>
    public abstract class Registration
    {

        /// <summary>
        /// The client id to associate the registration.
        /// </summary>
        public String Idclient { get; protected set; }

        /// <summary>
        /// The subscription type of registration.
        /// </summary>
        public SubscriptionType type { get; protected set; }

        /// <summary>
        /// The registration'discount.
        /// </summary>
        public Double discount { get; protected set; }

        /// <summary>
        /// Method to get the price of the registration.
        /// </summary>
        /// <returns> The price </returns>
        public abstract Double GetPrice();

        /// <summary>
        /// Method to check if a registration is still active on a specific date.
        /// </summary>
        /// <param name="date"> The date </param>
        /// <returns> True if is active, otherwise false </returns>
        public abstract bool IsActive(DateTime date);

        /// <summary>
        /// Method to check if duration is a positive integer
        /// </summary>
        /// <param name="duration"> The duration </param>
        /// <returns> True if it is correct, otherwise false </returns>
        public static bool IsDurationValid(int duration)
        {
            return duration > 0;
        }

        /// <summary>
        /// Method to check if the discount is between 0 and 100.
        /// </summary>
        /// <param name="discount"> The discount </param>
        /// <returns> True if it is correct, otherwise false </returns>
        public static bool IsDiscountValid(Double discount)
        {
            return discount <= 100 && discount >= 0;
        }

        /// <summary>
        /// Method to check if the signing date is before today.
        /// </summary>
        /// <param name="date"> The signing date </param>
        /// <returns> True if it is correct, otherwise false </returns>
        public static bool IsSigningDateValid(DateTime date)
        {
            return DateTime.Compare(date, DateTime.Now) > 0;
        }
    }
}