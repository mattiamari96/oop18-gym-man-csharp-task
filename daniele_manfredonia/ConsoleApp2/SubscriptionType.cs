﻿using System;





namespace ConsoleApp2
{
    /// <summary>
    /// The Class SubscriptionType implements the concept of subscription to which a customer can sign up.
    /// </summary>
    public class SubscriptionType
    {
        /// <summary>
        /// The subscription's name.
        /// </summary>
        public String name { get; protected set; }

        /// <summary>
        /// The subscription's description.
        /// </summary>
        public String description { get; protected set; }

        /// <summary>
        /// The subscription's price.
        /// </summary>
        public Double price { get; protected set; }
        
        /// <summary>
        /// Constructor to create a new subscription.
        /// </summary>
        /// <param name="name"> The name </param>
        /// <param name="description"> The description </param>
        /// <param name="price"> The price </param>
        public SubscriptionType(String name, String description, Double price)
        {
            this.name = name;
            this.description = description;
            this.price = price;
        }
    }
}
