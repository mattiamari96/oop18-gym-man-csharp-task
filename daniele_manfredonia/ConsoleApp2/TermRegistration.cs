﻿using System;

namespace ConsoleApp2
{
    /// <summary>
    /// The Class TermRegistration implements the concept of term registration.
    /// </summary>
    public class TermRegistration : Registration
    {
        /// <summary>
        /// The registration's signing date
        /// </summary>
        public DateTime signingDate { get; protected set; }

        /// <summary>
        /// The registration's duration.
        /// </summary>
        public int duration { get; protected set; }

        /// <summary>
        /// The empty constructor.
        /// </summary>
        private TermRegistration() { }
        
        /// <summary>
        /// The class to build the term registration. 
        /// </summary>
        public class Builder
        {
            private String Idclient;

            private SubscriptionType type;

            private Double discount;

            private DateTime signingDate;

            private int duration;

            /// <summary>
            /// Method to set registration'id client.
            /// </summary>
            /// <param name="IDclient"> The id client </param>
            /// <returns> The builder </returns>
            public Builder IDclient(String IDclient)
            {
                this.Idclient = IDclient;

                return this;
            }

            /// <summary>
            /// Method to set registration'subscription type.
            /// </summary>
            /// <param name="type"> The subscription type </param>
            /// <returns> The builder </returns>
            public Builder Type(SubscriptionType type)
            {
                if (type != null)
                {
                    this.type = type;
                }

                return this;
            }

            /// <summary>
            /// Method to set registration'discount.
            /// </summary>
            /// <param name="discount"> The discount </param>
            /// <returns> The builder </returns>
            public Builder Discount(Double discount)
            {
                if (IsDiscountValid(discount))
                {
                    this.discount = discount;
                }
                return this;
            }

            /// <summary>
            /// Method to set registration'signing date.
            /// </summary>
            /// <param name="signingDate"> The signing date </param>
            /// <returns> The builder </returns>
            public Builder SigningDate(DateTime signingDate)
            {
                if (IsSigningDateValid(signingDate))
                {
                    this.signingDate = signingDate;
                }
                return this;
            }

            /// <summary>
            /// Method to set registration'duration.
            /// </summary>
            /// <param name="duration"> The duration </param>
            /// <returns> The builder </returns>
            public Builder Duration(int duration)
            {
                if (IsDurationValid(duration))
                {
                    this.duration = duration;
                }
                return this;
            }

            /// <summary>
            /// Method to build the term registration.
            /// </summary>
            /// <returns> The new registration </returns>
            public TermRegistration Build()
            {
                TermRegistration termRegistration = new TermRegistration();

                termRegistration.Idclient = Idclient;
                termRegistration.type = type;
                termRegistration.signingDate = signingDate;
                termRegistration.duration = duration;
                termRegistration.discount = discount;

                return termRegistration;

            }
        }

        public override double GetPrice()
        {
            return this.type.price * this.duration * (1 - this.discount/100);
        }

        public override bool IsActive(DateTime date)
        {
            return DateTime.Compare(this.signingDate.AddMonths(this.duration), date) >= 0;
        }
    }
}
