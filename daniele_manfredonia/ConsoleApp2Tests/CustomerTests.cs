﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.Tests
{
    [TestClass()]
    public class CustomerTests
    {
        [TestMethod()]
        public void IsNameLiteralTest()
        {
            String correctName = "daniele";

            String wrongName = "dani98";

            Assert.IsTrue(Customer.IsNameValid(correctName));

            Assert.IsFalse(Customer.IsNameValid(wrongName));
        }
        
        public void IsFiscalcodeValidTest()
        {
            String correctFiscalcode = "ASDASD45S34S456d";
            String wrongFiscalcode = "addadasdaddasdasd";

            Assert.IsTrue(Customer.IsFiscalcodeValid(correctFiscalcode));

            Assert.IsFalse(Customer.IsFiscalcodeValid(wrongFiscalcode));
        }

        [TestMethod()]
        public void IsIdValidTest()
        {
            String correctID = "adsssssaasdsa";

            String wrongID = "sda";

            Assert.IsTrue(Customer.IsIdValid(correctID));

            Assert.IsFalse(Customer.IsIdValid(wrongID));
        }

        [TestMethod()]
        public void IsEmailValidTest()
        {
            String correctEmail = "daniele@gmail.com";

            String wrongEmail = "adasdas";

            String otherWrongEmail = "asda@sada";

            Assert.IsTrue(Customer.IsEmailValid(correctEmail));

            Assert.IsFalse(Customer.IsEmailValid(wrongEmail));

            Assert.IsFalse(Customer.IsEmailValid(otherWrongEmail));
        }

        [TestMethod()]
        public void IsTelephoneNumberLengthValidTest()
        {
            String correctNumber = "3316947329";

            String wrongNumber = "333";

            Assert.IsTrue(Customer.IsTelephoneNumberValid(correctNumber));

            Assert.IsFalse(Customer.IsTelephoneNumberValid(wrongNumber));
        }

        [TestMethod()]
        public void IsBirthdateBeforeTodayTest()
        {
            DateTime correctBirthdate = DateTime.Now.AddDays(-2);

            DateTime wrongBirthdate = DateTime.Now.AddDays(1); ;

            Assert.IsTrue(Customer.IsBirthdateValid(correctBirthdate));

            Assert.IsFalse(Customer.IsBirthdateValid(wrongBirthdate));
        }
    }
}