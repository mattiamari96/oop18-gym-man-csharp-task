﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.Tests
{


    [TestClass()]
    public class NumberedRegistrationTests
    {

        Customer customer = new Customer.Builder()
                                .ID("asdfghjkl")
                                .Firstname("Daniele")
                                .Lastname("Manfredonia")
                                .Gender(Customer.Gender.MALE)
                                .Birthdate(new DateTime(1998, 07, 10))
                                .Fiscalcode("RSSMRA80A01H199L")
                                .TelephoneNumber("3313315684")
                                .Email("daniele@gmail.com")
                                .Build();


        SubscriptionType subscription = new SubscriptionType("Sala pesi", "Tirare su pesi", 10);

        [TestMethod()]
        public void IsActiveTest()
        {

            NumberedRegistration numberedRegistration = new NumberedRegistration.Builder()
                                                    .IDclient(customer.id)
                                                    .Type(subscription)
                                                    .MaxEntries(3)
                                                    .Discount(10)
                                                    .Build();
            numberedRegistration.AddCountEntries();
            numberedRegistration.AddCountEntries();


            Assert.IsTrue(numberedRegistration.IsActive(DateTime.Now));

            numberedRegistration.AddCountEntries();

            Assert.IsFalse(numberedRegistration.IsActive(DateTime.Now));
        }
    }
}