﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.Tests
{
    [TestClass()]
    public class RegistrationTests
    {
        [TestMethod()]
        public void IsDurationValidTest()
        {
            int correctDuration = 10;

            int wrongDuration = -2;

            Assert.IsTrue(Registration.IsDurationValid(correctDuration));

            Assert.IsFalse(Registration.IsDurationValid(wrongDuration));
        }

        [TestMethod()]
        public void IsDiscountValidTest()
        {
            double correctDiscount = 10;

            double wrongDiscount = -2;

            double otherWrongDiscount = 120;

            Assert.IsTrue(Registration.IsDiscountValid(correctDiscount));

            Assert.IsFalse(Registration.IsDiscountValid(wrongDiscount));

            Assert.IsFalse(Registration.IsDiscountValid(otherWrongDiscount));
        }

        [TestMethod()]
        public void IsSigningDateValidTest()
        {
            DateTime correctSigningDate = DateTime.Now.AddDays(+1);

            DateTime wrongSigningDate = DateTime.Now.AddDays(-1);

            

            Assert.IsTrue(Registration.IsSigningDateValid(correctSigningDate));

            Assert.IsFalse(Registration.IsSigningDateValid(wrongSigningDate));

        }
    }
}