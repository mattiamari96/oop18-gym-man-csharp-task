﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection.Emit;

namespace ConsoleApp2.Tests
{
    [TestClass()]
    public class TermRegistrationTests
    {

        Customer customer = new Customer.Builder()
                                .ID("asdfghjkl")
                                .Firstname("Daniele")
                                .Lastname("Manfredonia")
                                .Gender(Customer.Gender.MALE)
                                .Birthdate(new DateTime(1998,07,10))
                                .Fiscalcode("RSSMRA80A01H199L")
                                .TelephoneNumber("3313315684")
                                .Email("daniele@gmail.com")
                                .Build();


        SubscriptionType subscription = new SubscriptionType("Sala pesi", "Tirare su pesi", 10);


        [TestMethod()]
        public void IsActiveTest()
        {

            TermRegistration termRegistration = new TermRegistration.Builder()
                                                    .IDclient(customer.id)
                                                    .Type(subscription)
                                                    .SigningDate(new DateTime(2020, 6, 1))
                                                    .Duration(10)
                                                    .Discount(10)
                                                    .Build();
            
            Assert.IsTrue(termRegistration.IsActive(new DateTime(2020, 7, 1)));

            Assert.IsFalse(termRegistration.IsActive(new DateTime(2025, 7, 1)));

        }

        [TestMethod()]
        public void GetPriceTest()
        {

            TermRegistration termRegistration = new TermRegistration.Builder()
                                                    .IDclient(customer.id)
                                                    .Type(subscription)
                                                    .SigningDate(new DateTime(2020, 6, 1))
                                                    .Duration(10)
                                                    .Discount(10)
                                                    .Build();

            Assert.AreEqual(termRegistration.GetPrice(), 90.0);


        }
    }
}