﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    /// <summary>
    ///  The Class Bmi implements a Bmi for users.
    /// </summary>
    public class Bmi
    {
        public string id { get; protected set; }
        public string customerid { get; protected set; }
        public double weight { get; protected set; }
        public double height { get; protected set; }
        public DateTime date { get; protected set; }
        private Bmi() { }

        /// <summary>
        /// The Class to build the Bmi.
        /// </summary>
        public class Builder
        {
            public string id;
            public string customerid;
            public double weight;
            public double height;
            public DateTime date;

            /// <summary>
            /// Method to set the Bmi's id.
            /// </summary>
            /// <param name="id"> The ID </param>
            /// <returns> The builder </returns>
            public Builder ID(string id)
            {
                if (IsIdValid(id))
                {
                    this.id = id;
                }

                return this;
            }

            /// <summary>
            /// Method to set the Bmi's customerid.
            /// </summary>
            /// <param name="customerid"> The customerid </param>
            /// <returns> The builder </returns>
            public Builder Customerid(string customerid)
            {
                if (IsIdValid(customerid))
                {
                    this.customerid = customerid;
                }
                return this;
            }

            /// <summary>
            /// Method to set the Bmi's weight.
            /// </summary>
            /// <param name="weight"> The weight </param>
            /// <returns> The builder </returns>
            public Builder Weight(double weight)
            {
                if (CheckValueWeight(weight))
                {
                    this.weight = weight;
                }
                return this;
            }

            /// <summary>
            /// Method to set the Bmi's height.
            /// </summary>
            /// <param name="height"> The height </param>
            /// <returns> The builder </returns>
            public Builder Height(double height)
            {
                if (CheckValueHeight(height))
                {
                    this.height = height;
                }
                return this;
            }

            /// <summary>
            /// Method to set the Bmi's date.
            /// </summary>
            /// <param name="date"> The date </param>
            /// <returns> The builder </returns>
            public Builder Date(DateTime date)
            {
                if (CheckValueDate(date))
                {
                    this.date = date;
                }
                return this;
            }

            /// <summary>
            /// Method to build the Bmi
            /// </summary>
            /// <returns> The Bmi build </returns>
            public Bmi Build()
            {
                Bmi bmi = new Bmi();

                bmi.id = id;
                bmi.customerid = customerid;
                bmi.weight = weight;
                bmi.height = height;
                bmi.date = date;

                return bmi;
            }
        }

        /// <summary>
        /// Method to check if the lenght of id is correct
        /// </summary>
        /// <param name="id"></param>
        /// <returns> True if it is correct, otherwise false </returns>
        public static bool IsIdValid(string id)
        {
            return id.Length >= 8;
        }

        /// <summary>
        /// Method to check if the value of weight of user is correct
        /// </summary>
        /// <param name="weight"></param>
        /// <returns> True if it is correct, otherwise false </returns>
        public static bool CheckValueWeight(double weight)
        {
            return weight >= 20 && weight <= 300;
        }

        /// <summary>
        /// Method to check if the value of height of user is correct
        /// </summary>
        /// <param name="height"></param>
        /// <returns> True if it is correct, otherwise false </returns>
        public static bool CheckValueHeight(double height)
        {
            return height >= 0.50 && height <= 2.40;
        }

        /// <summary>
        /// Method to check if parameter date is after today. 
        /// </summary>
        /// <param name="date"> The date </param>
        /// <returns> True if it is correct, otherwise false </returns>
        public static bool CheckValueDate(DateTime date)
        {
            return date.CompareTo(DateTime.Today) > 0;
        }
    }
}
