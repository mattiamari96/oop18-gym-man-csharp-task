﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    /// <summary>
    ///  The Class Tool implements a tool for use to gym.
    /// </summary>
    public class Tool
    {
        public String id { get; protected set; }
        public String name { get; protected set; }
        public String desc { get; protected set; }
        public String numseriale { get; protected set; }
        public int maintenance { get; protected set; }

        private Tool() { }

        /// <summary>
        /// The Class to build the Tool.
        /// </summary>
        public class Builder
        {
            public String id;
            public String name;
            public String desc;
            public String numseriale;
            public int maintenance;

            /// <summary>
            /// Method to set the Tool's id.
            /// </summary>
            /// <param name="id"> The ID </param>
            /// <returns> The builder </returns>
            public Builder ID(String id)
            {
                if (IsIdValid(id))
                {
                    this.id = id;
                }

                return this;
            }

            /// <summary>
            /// Method to set the Tool's name.
            /// </summary>
            /// <param name="name"> The name </param>
            /// <returns> The builder </returns>
            public Builder Name(String name)
            {
                if (IsEmptyValue(name))
                {
                    this.name = name;
                }
                return this;
            }

            /// <summary>
            /// Method to set the Tool's numseriale.
            /// </summary>
            /// <param name="numseriale"> The numseriale </param>
            /// <returns> The builder </returns>
            public Builder Numseriale(String numseriale)
            {
                if (IsEmptyValue(numseriale))
                {
                    this.numseriale = numseriale;
                }
                return this;
            }

            /// <summary>
            /// Method to set the Tool's maintenance.
            /// </summary>
            /// <param name="maintenance"> The maintenance </param>
            /// <returns> The builder </returns>
            public Builder Maintenance(int maintenance)
            {
                if (CheckValue(maintenance))
                {
                    this.maintenance = maintenance;
                }
                return this;
            }

            /// <summary>
            /// Method to build the Tool
            /// </summary>
            /// <returns> The Tool build </returns>
            public Tool Build()
            {
                Tool tool = new Tool();

                tool.id = id;
                tool.name = name;
                tool.numseriale = numseriale;
                tool.maintenance = maintenance;
                tool.desc = desc;

                return tool;
            }
        }

        /// <summary>
        /// Method to check if the lenght of id is correct
        /// </summary>
        /// <param name="id"></param>
        /// <returns> True if it is correct, otherwise false </returns>
        public static bool IsIdValid(String id)
        {
            return id.Length >= 8;
        }

        /// <summary>
        /// Method to check if the parameter is Empty or not.
        /// </summary>
        /// <param name="name"> The name </param>
        /// <param name="numseriale"> The numseriale </param>
        /// <returns> True if it is correct, otherwise false </returns>
        public static bool IsEmptyValue(String tmp)
        {
            return tmp.Trim().Length > 1;
        }

        /// <summary>
        /// Method to check if the value of maintenance is correct
        /// </summary>
        /// <param name="maintenance"></param>
        /// <returns> True if it is correct, otherwise false </returns>
        public static bool CheckValue(int maintenance)
        {
            return maintenance <= 12 && maintenance >= 0;
        }

    }

}
