﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Tests
{
    [TestClass()]
    public class BmiTests
    {
        [TestMethod()]
        public void IsIdValidTest()
        {
            String correctID = "abcdefght";
            String wrongID = "asd";

            Assert.IsTrue(Bmi.IsIdValid(correctID));
            Assert.IsFalse(Bmi.IsIdValid(wrongID));
        }

        [TestMethod()]
        public void CheckValueWeightTest()
        {
            Double correctWeight = 100;
            Double wrongWeight = 15;

            Assert.IsTrue(Bmi.CheckValueWeight(correctWeight));
            Assert.IsFalse(Bmi.CheckValueWeight(wrongWeight));
        }

        [TestMethod()]
        public void CheckValueHeightTest()
        {
            Double correctHeight = 2;
            Double wrongHeight = 4;

            Assert.IsTrue(Bmi.CheckValueHeight(correctHeight));
            Assert.IsFalse(Bmi.CheckValueHeight(wrongHeight));
        }

        [TestMethod()]
        public void CheckValueDateTest()
        {
            DateTime correctDate = DateTime.Now.AddDays(1);
            DateTime wrongDate = DateTime.Now.AddDays(-5);

            Assert.IsTrue(Bmi.CheckValueDate(correctDate));
            Assert.IsFalse(Bmi.CheckValueDate(wrongDate));
        }
    }
}