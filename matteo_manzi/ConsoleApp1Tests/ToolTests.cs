﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Tests
{
    [TestClass()]
    public class ToolTests
    {
        [TestMethod()]
        public void IsIdValidTest()
        {
            String correctID = "abcdefght";
            String wrongID = "asd";

            Assert.IsTrue(Tool.IsIdValid(correctID));
            Assert.IsFalse(Tool.IsIdValid(wrongID));
        }

        [TestMethod()]
        public void IsEmptyValueTest()
        {
            String othercorrectNumSeriale = "ASDadjada13";
            String correctMaintenance = "Tapirulan";
            String wrongMaintenance = "";
            String othercorrectMaintenance = "asd  asdad";
            String otherwrongMaintenance = "     ";

            Assert.IsTrue(Tool.IsEmptyValue(othercorrectNumSeriale));
            Assert.IsTrue(Tool.IsEmptyValue(correctMaintenance));
            Assert.IsTrue(Tool.IsEmptyValue(othercorrectMaintenance));
            Assert.IsFalse(Tool.IsEmptyValue(wrongMaintenance));
            Assert.IsFalse(Tool.IsEmptyValue(otherwrongMaintenance));
        }

        [TestMethod()]
        public void CheckValueTest()
        {
            int correctMaintenance = 5;
            int wrongMaintenance = 15;

            Assert.IsTrue(Tool.CheckValue(correctMaintenance));
            Assert.IsFalse(Tool.CheckValue(wrongMaintenance));
        }
    }
}