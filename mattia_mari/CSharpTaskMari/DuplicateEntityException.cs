﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTaskMari
{
    public class DuplicateEntityException : Exception
    {
        public DuplicateEntityException(IEntity existing, IEntity duplicate)
            : base(String.Format("Duplicate entity %s. Already existing: %s", duplicate.ToString(), existing.ToString())) { }
    }
}
