﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTaskMari
{
    /// <summary>
    /// Generic Entity. All entities must have a unique id
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Get the ID for this entity
        /// </summary>
        /// <returns>ID</returns>
        string GetId();
    }
}
