﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTaskMari
{
    /// <summary>
    /// Base interface for repositories with the classic CRUD methods
    /// </summary>
    /// <typeparam name="T">Some type that implements IEntity</typeparam>
    public interface IRepository<T> where T : IEntity
    {
        /// <summary>
        /// Add an Entity
        /// </summary>
        /// <param name="entity">The entity</param>
        void Add(T entity);

        /// <summary>
        /// Remove an entity. It will be matched by its ID
        /// </summary>
        /// <param name="entity">The entity</param>
        void Remove(T entity);

        /// <summary>
        /// Check if the repository contains an entity. It will be matched by its ID
        /// </summary>
        /// <param name="entity">The entity</param>
        /// <returns>true if found, false otherwise</returns>
        bool Contains(T entity);

        /// <summary>
        /// Get an entity by ID
        /// </summary>
        /// <param name="id">Entity ID</param>
        /// <returns>The entity if found, null otherwise</returns>
        T Get(string id);

        /// <summary>
        /// Count the contents of the repository
        /// </summary>
        /// <returns>The number of entities contained</returns>
        int Count();
    }
}
