﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTaskMari
{
    interface IUserRepository : IRepository<User>
    {
        /// <summary>
        /// Find a user by its username
        /// </summary>
        /// <param name="username">The username</param>
        /// <returns>An instance of User if found, null otherwise</returns>
        User FindByUsername(string username);
    }
}
