﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTaskMari
{
    /// <summary>
    /// Implementation of a basic repository which stores its contents in memory
    /// </summary>
    /// <typeparam name="T">Some Entity subclass</typeparam>
    public abstract class MemoryRepository<T> : IRepository<T> where T : IEntity
    {
        protected List<T> items = new List<T>();

        public void Add(T entity)
        {
            T existing = Get(entity.GetId());
            
            if (existing != null)
            {
                items.Remove(existing);
            }

            items.Add(entity);
        }

        public bool Contains(T entity)
        {
            return items.Contains(entity);
        }

        public int Count()
        {
            return items.Count;
        }

        public T Get(string id)
        {
            return items.Find(e => e.GetId() == id);
        }

        public void Remove(T entity)
        {
            items.Remove(entity);
        }
    }
}
