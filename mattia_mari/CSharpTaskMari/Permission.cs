﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTaskMari
{
    /// <summary>
    /// A Permission that can be assigned to a User through a Role
    /// </summary>
    public class Permission
    {
        /// <summary>
        /// Name of the Permission. Serves as unique ID too
        /// </summary>
        public string name { get; }

        /// <summary>
        /// Human readable description
        /// </summary>
        public string description { get; }

        public Permission(string name) : this(name, "") { }

        public Permission(string name, string description)
        {
            this.name = name;
            this.description = description;
        }

        public override bool Equals(Object other)
        {
            if (other == null || !this.GetType().Equals(other.GetType())) {
                return false;
            }

            return name == ((Permission)other).name;
        }

        public override int GetHashCode()
        {
            return name.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("%s: %s", name, description);
        }
    }
}
