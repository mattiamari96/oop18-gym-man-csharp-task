﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTaskMari
{
    /// <summary>
    /// A Role that can be assigned to a User
    /// </summary>
    public class Role
    {
        /// <summary>
        /// Name of the Role. Serves as unique ID too
        /// </summary>
        public string name { get; }

        /// <summary>
        /// Human readable description
        /// </summary>
        public string description { get; }

        private ISet<Permission> permissions = new HashSet<Permission>();

        public Role(string name, string description, ISet<Permission> permissions)
        {
            this.name = name;
            this.description = description;
            
            foreach (Permission p in permissions)
            {
                this.permissions.Add(p);
            }
        }

        public Role(string name, string description) : this(name, description, new HashSet<Permission>()) { }

        public Role(string name) : this(name, "") { }

        /// <summary>
        /// Check if this Role contains a Permission
        /// </summary>
        /// <param name="permission">The Permission to check for</param>
        /// <returns>true if found, false otherwise</returns>
        public bool HasPermission(Permission permission)
        {
            return permissions.Contains(permission);
        }

        /// <summary>
        /// Add a Permission to this Role
        /// </summary>
        /// <param name="permission">The permission</param>
        public void AddPermission(Permission permission)
        {
            permissions.Add(permission);
        }

        /// <summary>
        /// Remove a permission from this role
        /// </summary>
        /// <param name="permission"></param>
        public void RemovePermission(Permission permission)
        {
            permissions.Remove(permission);
        }

        /// <summary>
        /// Get this role's set of permissions. It's a copy so it's safe to modify.
        /// </summary>
        /// <returns>A copy of this role's set of permissions</returns>
        public ISet<Permission> GetPermissions()
        {
            return new HashSet<Permission>(permissions);
        }

        public override string ToString()
        {
            return String.Format("%s: %s", name, description);
        }

        public override int GetHashCode()
        {
            return name.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }

            return name == ((Role)obj).name;
        }
    }
}
