﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTaskMari
{
    /// <summary>
    /// A User of the system
    /// </summary>
    public class User : IEntity
    {
        /// <summary>
        /// User's unique ID
        /// </summary>
        public string id { get; }

        /// <summary>
        /// Username
        /// </summary>
        public string name { get; }

        public User(string id, string name)
        {
            this.id = id;
            this.name = name;
        }

        public string GetId()
        {
            return id;
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }

            return id == ((User)obj).GetId();
        }

        public override string ToString()
        {
            return String.Format("User(id='%s' name='%s')", id, name);
        }
    }
}
