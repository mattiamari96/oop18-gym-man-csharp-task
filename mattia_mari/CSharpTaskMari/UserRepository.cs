﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTaskMari
{
    /// <summary>
    /// The repository for Users
    /// </summary>
    public class UserRepository : MemoryRepository<User>, IUserRepository
    {
        public new void Add(User user)
        {
            User existing = items.Find(e => e.id != user.id && e.name == user.name);

            if (existing != null)
            {
                throw new DuplicateEntityException(existing, user);
            }

            base.Add(user);
        }

        public User FindByUsername(string username)
        {
            return items.Find(e => e.name == username);
        }
    }
}
