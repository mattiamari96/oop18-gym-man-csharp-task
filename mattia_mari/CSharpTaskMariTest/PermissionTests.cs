﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CSharpTaskMari;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTaskMari.Tests
{
    [TestClass()]
    public class PermissionTests
    {
        [TestMethod()]
        public void EqualsTest()
        {
            Permission p1 = new Permission("perm1");
            Permission p11 = new Permission("perm1");
            Permission p2 = new Permission("perm2");

            Assert.AreEqual(p1, p11);
            Assert.AreNotEqual(p1, p2);
        }
    }
}