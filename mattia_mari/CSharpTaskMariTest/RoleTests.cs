﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CSharpTaskMari;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTaskMari.Tests
{
    [TestClass()]
    public class RoleTests
    {
        [TestMethod()]
        public void hasPermissionTest()
        {
            Permission p = new Permission("perm");
            Role r = new Role("role");

            Assert.IsFalse(r.HasPermission(p));

            r.AddPermission(p);

            Assert.IsTrue(r.HasPermission(p));
        }

        [TestMethod()]
        public void GetPermissionsTest()
        {
            Permission p = new Permission("perm");
            Role r = new Role("role");

            r.AddPermission(p);

            Assert.AreEqual(1, r.GetPermissions().Count);
        }
    }
}