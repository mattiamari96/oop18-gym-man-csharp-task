﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CSharpTaskMari;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTaskMari.Tests
{
    [TestClass()]
    public class UserRepositoryTests
    {
        [TestMethod()]
        public void AddTest()
        {
            User u1 = new User("u1", "user1");
            User u11 = new User("u1", "user11");

            UserRepository repo = new UserRepository();

            repo.Add(u1);
            Assert.IsTrue(repo.Contains(u1));
            Assert.IsTrue(repo.Contains(u11));

            Assert.AreSame(u1, repo.Get("u1"));
            repo.Add(u11);
            Assert.AreNotSame(u1, repo.Get("u1"));
            Assert.AreSame(u11, repo.Get("u1"));
        }

        [TestMethod()]
        [ExpectedException(typeof(DuplicateEntityException))]
        public void AddDuplicateUserTest()
        {
            User u1 = new User("u1", "user1");
            User u2 = new User("u2", "user1");

            UserRepository repo = new UserRepository();

            repo.Add(u1);
            repo.Add(u2);
        }

        [TestMethod()]
        public void FindByUsernameTest()
        {
            User u1 = new User("u1", "user1");
            UserRepository repo = new UserRepository();

            repo.Add(u1);

            Assert.AreSame(u1, repo.FindByUsername("user1"));
        }
    }
}