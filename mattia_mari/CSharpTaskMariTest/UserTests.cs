﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CSharpTaskMari;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTaskMari.Tests
{
    [TestClass()]
    public class UserTests
    {
        [TestMethod()]
        public void GetHashCodeTest()
        {
            User u1 = new User("u1", "user1");
            User u11 = new User("u1", "user1");

            Assert.AreEqual(u1.GetHashCode(), u11.GetHashCode());
        }

        [TestMethod()]
        public void EqualsTest()
        {
            User u1 = new User("u1", "user1");
            User u11 = new User("u1", "user1");

            Assert.AreEqual(u1, u11);
        }
    }
}